package test.com.maBanque;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;  
import com.maBanque.CompteImpl; 
 
public class CompteTest { 
	
	CompteImpl compte;
	
	@Before
	
	public void init() {
		
		 compte = new CompteImpl();
		
	}
 	 
	@Test 
 	public void setSolde(){ 
 	 	try{ 
 	 	 	 		 	 	
 	 	 	compte.setSolde(30);  	 	 	
 	 	 	float solde = compte.getSolde();  	 	
 	 	 	assertTrue(solde == 30);  	 	
 	 	 } catch(Exception e){ 
 	 	 	fail(e.getMessage()); 
 	 	} 
	}
	
	@Test 
 	public void debiter(){ 
 	 	try{ 
 	 	 	 		 	 	
 	 	 	compte.setSolde(30);  	 	 	
 	 	 	compte.debiter(50);  	
 	 	 	float solde = compte.getSolde();
 	 	 	assertTrue(solde >=0);  	 	
 	 	 } catch(Exception e){ 
 	 	   fail(e.getMessage()); 
 	 	} 
	}
	
	@Test 
 	public void crediter(){ 
 	 	try{ 
 	 	 	 		 	 	
 	 	 	compte.setSolde(10);  	 	 	
 	 	    compte.crediter(11);  
 	 	    float solde = compte.getSolde();
 	 	 	assertTrue(solde >= 0);  	 	
 	 	 } catch(Exception e){ 
 	 	 	fail(e.getMessage()); 
 	 	} 
	}
 	 	
 	 	
	@Test(expected = Exception.class)
 	 public void setSoldeErreur() throws Exception {
		compte.setSolde(-2);
 	 
 	} 
	
	@Test(expected = Exception.class)
	 public void crediterErreur() throws Exception {
		compte.crediter(-8);
	 
	} 
	
	@Test(expected = Exception.class)
	 public void debiterErreur() throws Exception {
		compte.setSolde(30);
		compte.debiter(18);
	
	 
	} 



} 


