package com.maBanque;

public class CompteImpl implements Compte{ 
	
	public CompteImpl() {
		
		solde = 0;
	}
 
 	float solde; 
 	 
 	@Override 
 	public void crediter(float credit) throws Exception{ 
    if (credit < 0) 
    	throw new Exception();
    	
    	else
    		this.solde += credit;
    	
 	
 	}

	@Override
	public float getSolde() {
		return this.solde;
	}

	@Override
	public float debiter(float debit) throws Exception {
		if (debit > this.solde) {
		
			return this.solde;
		}
	
		else if (debit < 20 || debit > 1000 ) {
	    	throw new Exception();
	 
		}
		
		else if (this.solde > debit) {
			return this.solde;
		}
		
		return debit;

	}

	
	@Override
	public void setSolde(float solde) throws Exception {
		
		if(solde <= 0) {
			
			throw new Exception();
		}
		this.solde = solde;
	} 
  
} 
